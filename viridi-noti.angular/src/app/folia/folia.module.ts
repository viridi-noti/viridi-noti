import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CoreModule,
  ],
  exports: [
    HomeComponent,
    CoreModule,
  ]
})
export class FoliaModule {}
