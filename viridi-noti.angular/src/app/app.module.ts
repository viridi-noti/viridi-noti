import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './modules/app-routing.module';
import { PrimeNgModule } from './modules/primeng.module';
import { FoliaModule } from './folia/folia.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    FoliaModule,
    AppRoutingModule,
    PrimeNgModule,
    BrowserModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
