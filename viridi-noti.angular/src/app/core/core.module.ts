import { NgModule } from '@angular/core';
import { HeaderComponent } from './component/header/header.component';

@NgModule({
  declarations: [
    HeaderComponent,
  ],
  exports: [
    HeaderComponent,
  ]
})
export class CoreModule {}
