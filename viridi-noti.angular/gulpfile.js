const gulp = require('gulp');
const path = require('path');
const del = require('del');

const COPY_DIST_TASK = 'copy_dist';
const BUILD_DIR = path.join(__dirname, 'dist');
const DEPLOY_DIR = path.join(__dirname, '..', '..', 'viridi-noti.github.io');
const BUILD_FILES = path.join(BUILD_DIR, '**', '*.*');
const DEPLOY_CLEAN_FILES = path.join(DEPLOY_DIR, '**', '*.*');
const DEPLOY_IGNORE_FILES = path.join(DEPLOY_DIR, '.git', '**', '*.*');

gulp.task(COPY_DIST_TASK, () => {
  del(
    [DEPLOY_CLEAN_FILES, `!${DEPLOY_IGNORE_FILES}`],
    { force: true },
  ).then(() => {
    gulp.src(BUILD_FILES).pipe(gulp.dest(DEPLOY_DIR));
  });
});

gulp.task('default', [COPY_DIST_TASK]);
